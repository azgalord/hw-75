import React, { Component } from 'react';
import './App.css';
import Decoder from "./containers/Decoder/Decoder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Decoder/>
      </div>
    );
  }
}

export default App;
