import React, {Component} from 'react';
import './Decoder.css';
import {sendMessageForDecode, sendMessageForEncode} from "../../store/actions/decoderActions";
import {connect} from 'react-redux';

class Decoder extends Component {
  state = {
    decode: '',
    encode: '',
    password: '',
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.encode !== this.props.encodedMessage && this.props.encodedMessage !== prevProps.encodedMessage) {
      this.setState({encode: this.props.encodedMessage});
    }
    if (this.state.decode !== this.props.decodedMessage && this.props.decodedMessage !== prevProps.decodedMessage) {
      this.setState({decode: this.props.decodedMessage});
    }
  }

  changeHandler = event => {
    this.setState({[event.target.name] : event.target.value});
  };

  decodeMessage = event => {
    event.preventDefault();
    if (this.state.password) {
      const message = {
        "password" : this.state.password,
        "message" : this.state.encode,
      };
      this.props.sendMessageForDecode(message);
    }
  };

  encodeMessage = event => {
    event.preventDefault();
    if (this.state.password) {
      const message = {
        "password" : this.state.password,
        "message" : this.state.decode,
      };
      this.props.sendMessageForEncode(message);
    }
  };

  render() {
    return (
      <div className="Decoder">
        <h2 style={{marginBottom: '40px', textTransform: 'uppercase'}}>Decoder app</h2>
        <form>
          <div className="DecoderTextArea">
            <label htmlFor="decode">Decoded message</label>
            <textarea
              onChange={event => this.changeHandler(event)}
              value={this.state.decode}
              name="decode" id="decode" cols="30" rows="10"
            />
          </div>
          <div className="DecoderTextArea">
            <label htmlFor="password">Password</label>
            <input
              onChange={event => this.changeHandler(event)}
              value={this.state.password}
              type="text" name="password"
            />
            <button onClick={(event) => this.encodeMessage(event)} className="EncodeMessage"/>
            <button onClick={(event) => this.decodeMessage(event)} className="DecodeMessage"/>
          </div>
          <div className="DecoderTextArea">
            <label htmlFor="encode">Encoded message</label>
            <textarea
              onChange={event => this.changeHandler(event)}
              value={this.state.encode}
              name="encode" id="encode" cols="30" rows="10"
            />
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  decodedMessage: state.decodedMessage,
  encodedMessage: state.encodedMessage
});

const mapDispatchToProps = dispatch => ({
  sendMessageForDecode: message => dispatch(sendMessageForDecode(message)),
  sendMessageForEncode: message => dispatch(sendMessageForEncode(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(Decoder);