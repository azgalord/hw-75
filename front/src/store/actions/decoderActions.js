import axios from '../../axios-decoder';

export const DECODE_MESSAGE = 'DECODE_MESSAGE';
export const ENCODE_MESSAGE = 'ENCODE_MESSAGE';

export const decodeMessage = message => ({type: DECODE_MESSAGE, message});
export const encodeMessage = message => ({type: ENCODE_MESSAGE, message});

export const sendMessageForDecode = message => {
  return dispatch => {
    axios.post('/decode', message).then(
      response => dispatch(decodeMessage(response.data)),
    )
  }
};

export const sendMessageForEncode = message => {
  return dispatch => {
    axios.post('/encode', message).then(
      response => dispatch(encodeMessage(response.data)),
    )
  }
};