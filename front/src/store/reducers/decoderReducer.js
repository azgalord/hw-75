import {DECODE_MESSAGE, ENCODE_MESSAGE} from "../actions/decoderActions";

const initialState = {
  decodedMessage: '',
  encodedMessage: '',
};

const decoderReducer = (state = initialState, action) => {
  switch (action.type) {
    case DECODE_MESSAGE:
      return {...state, decodedMessage: action.message.decoded};
    case ENCODE_MESSAGE:
      return {...state, encodedMessage: action.message.encoded};
    default:
      return state;
  }
};

export default decoderReducer;