const express = require('express');
const fs = require('fs');
const Caesar = require('caesar-salad').Caesar;
const router = express.Router();
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors());
app.use(router);
const port = 8000;

const password = 'azgalord94';

router.get('/', (req, res) => {
  res.send('Enter password');
});

// app.get('/decode/:password', (req, res) => {
//   const password = Caesar.Decipher('c').crypt(req.params.password);
//   res.send(password);
// });

router.post('/encode', (req, res) => {
  if (req.body.password === password) {
    const message = Caesar.Cipher('c').crypt(req.body.message);
    res.send({"encoded" : message});
  } else {
    res.send({"encoded" : "password is not correct!"});
  }
});

router.post('/decode', (req, res) => {
  if (req.body.password === password) {
    const message = Caesar.Decipher('c').crypt(req.body.message);
    res.send({"decoded" : message});
  } else {
    res.send({"decoded" : "password is not correct!"});
  }
});

app.listen(port, () => {
  console.log('We are on port ' + port);
});